import React from "react";

export const HeaderCaption = () => {
  return (
    <div className="bg-mobile-header bg-cover h-36 md:h-40 bg-no-repeat md:bg-desktop-header flex flex-col items-center justify-center">
      <p className="text-2xl mb-4">JOB Listing Application</p>
      <p>Adding Continuous Integration & Continuous Deployment</p>
    </div>
  );
};
