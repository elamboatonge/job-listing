import { render, screen } from "@testing-library/react";

// import App from "./App";
import { HeaderCaption } from "./components/HeaderCaption";

// test("renders learn react link", () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

test("renders header caption with correct text", () => {
  // const { getByText } =

  render(<HeaderCaption />);

  const textElement = screen.getByText("JOB Listing Application");
  console.log(textElement);
  expect(textElement).toBeInTheDocument();
});

// test('calls onClick handler when Clear button is clicked', () => {
//   const handleClick = jest.fn();
//   const { getByText } = render(<App />);
//   const buttonElement = getByText('Clear');
//   fireEvent.click(buttonElement);
//   expect(handleClick).toHaveBeenCalledTimes(1);
// });
